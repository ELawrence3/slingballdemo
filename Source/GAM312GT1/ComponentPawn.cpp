// Fill out your copyright notice in the Description page of Project Settings.

#include "ComponentPawn.h"
#include "CollidingPawnMovementComponent.h"


AComponentPawn::AComponentPawn()//removed "void" as return type may not be specified on a constructor
{
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	//PrimaryActorTick.bCanEverTick = true;

	AComponentPawn::Sphere = CreateDefaultSubobject<USphereComponent>(TEXT("RootComponent"));//fixed funky quotes
	RootComponent = Sphere;//Sets the root of the object to the collision component, so that all additional components are relatively located to it
	Sphere->InitSphereRadius(50.0f);//determines the radius of the invisible field which detects collision
	Sphere->SetCollisionProfileName(TEXT("Pawn"));//fixed funky quotes again.
	Sphere->SetNotifyRigidBodyCollision(true);//enable collision detection
	OnActorHit.AddDynamic(this, &AComponentPawn::OnHit);//create event of on hit call the OnHit function)

	UStaticMeshComponent* SphereMesh =
		CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Visual"));
	SphereMesh->AttachTo(RootComponent);
	static ConstructorHelpers::FObjectFinder<UStaticMesh>
		SphereVisualAsset(TEXT("/Game/StarterContent/Shapes/Shape_Sphere.Shape_Sphere"));//Sets the visable shape of the object
	if (SphereVisualAsset.Succeeded())
	{
		SphereMesh->SetStaticMesh(SphereVisualAsset.Object);
		SphereMesh->SetRelativeLocation(FVector(0.0f, 0.0f, -40.0f));//Sets the relative location of the visible portion. This indicates that the visable sphere is slightly higher than the center of the collision sphere, meaning there is more of a gap on the bottom than on the top. I'm unclear why this would be recommended as the default setting.
		SphereMesh->SetWorldScale3D(FVector(0.8f));//Sets the scale of the visable mesh in relation to the size of the entire object - in this case, the visable portion is 80% the size of the collision sphere.
	}
	// Create a particle system that we can activate or deactivate
	OurParticleSystem = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("MovementParticles"));
	OurParticleSystem->SetupAttachment(SphereMesh);
	OurParticleSystem->bAutoActivate = false;
	OurParticleSystem->SetRelativeLocation(FVector(-20.0f, 0.0f, 20.0f));
	static ConstructorHelpers::FObjectFinder<UParticleSystem> ParticleAsset(TEXT("/Game/StarterContent/Particles/P_Fire.P_Fire"));
	if (ParticleAsset.Succeeded())
	{
		OurParticleSystem->SetTemplate(ParticleAsset.Object);
	}

	// Use a spring arm to give the camera smooth, natural-feeling motion.
	USpringArmComponent* SpringArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraAttachmentArm"));
	SpringArm->SetupAttachment(RootComponent);
	SpringArm->RelativeRotation = FRotator(-45.f, 0.f, 0.f);
	SpringArm->TargetArmLength = 400.0f;
	SpringArm->bEnableCameraLag = true;
	SpringArm->CameraLagSpeed = 3.0f;
	// Create a camera and attach to our spring arm
	UCameraComponent* Camera = CreateDefaultSubobject<UCameraComponent>(TEXT("ActualCamera"));
	Camera->SetupAttachment(SpringArm, USpringArmComponent::SocketName);


	// Create an instance of our movement component, and tell it to update the root.
	OurMovementComponent = CreateDefaultSubobject<UCollidingPawnMovementComponent>(TEXT("CustomMovementComponent"));
	OurMovementComponent->UpdatedComponent = RootComponent;

	AutoPossessPlayer = EAutoReceiveInput::Player0;//sets the player to posess the inanimate object. "Hey, Hey Morty, look at me. I'M SPHERICAL RIIIIICK."  Sorry.
}

// Called when the game starts or when spawned
void AComponentPawn::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AComponentPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	//origionally I checked for overlapping actors every frame but this seemed expensive. Instead I switched to event driven collision detection
	//TArray<AActor*> actors;
	//SphereCollider->GetOverlappingActors(actors);
	//Sphere->OnComponentHit;

	//for (AActor* actor : actors) {
		//if(actor->GetFName() != GetFName())
		//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Orange, FString::Printf(TEXT("I just hit %s"), *actor->GetFName().ToString()));
	//}
}

void AComponentPawn::OnHit(AActor* SelfActor, AActor* OtherActor, FVector NormalImpulse, const FHitResult& Hit)
{
	if (!OtherActor->IsRootComponentStatic())//cannot pick up static objects, so why even try?
	{
		if (OtherActor->GetClass()->GetDefaultObjectName().ToString() == "Default__BP_Penman_C") {//Check if the non-static object is a Penman
			GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Orange, FString::Printf(TEXT("HALP! PUT ME DOWN RIGHT NOW!! (Press 'F' to put down)")));//Penmen don't like being picked up...
		}		
		
		OtherActor->AttachToActor(this, FAttachmentTransformRules(EAttachmentRule::KeepWorld, true));//attach the object at point of impact
		OtherActor->SetActorEnableCollision(false);//disable further collision. Without this you'll collide with your new "limb"
		heldActors.Add(OtherActor);
	}
	
}
// Called to bind functionality to input
void AComponentPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	InputComponent->BindAction("ParticleToggle", IE_Pressed, this, &AComponentPawn::ParticleToggle);
	InputComponent->BindAction("Release", IE_Pressed, this, &AComponentPawn::Release);

	//binds the events created in the input settings to the functions defined here
	InputComponent->BindAxis("MoveForward", this, &AComponentPawn::MoveForward);
	InputComponent->BindAxis("MoveRight", this, &AComponentPawn::MoveRight);
	InputComponent->BindAxis("Turn", this, &AComponentPawn::Turn);
	InputComponent->BindAxis("Look", this, &AComponentPawn::Look);

}

UPawnMovementComponent* AComponentPawn::GetMovementComponent() const
{
	return OurMovementComponent;
}

void AComponentPawn::MoveForward(float AxisValue)
{
	if (OurMovementComponent && (OurMovementComponent->UpdatedComponent == RootComponent))
	{
		OurMovementComponent->AddInputVector(GetActorForwardVector() * AxisValue);
	}
}

void AComponentPawn::MoveRight(float AxisValue)
{
	if (OurMovementComponent && (OurMovementComponent->UpdatedComponent == RootComponent))
	{
		OurMovementComponent->AddInputVector(GetActorRightVector() * AxisValue);
	}
}

void AComponentPawn::Turn(float AxisValue)//Allows the camera to rotate left and right using the mouse
{
	FRotator NewRotation = GetActorRotation();
	NewRotation.Yaw += AxisValue;
	SetActorRotation(NewRotation);
}
void AComponentPawn::Look(float AxisValue)//Allows the camera to look up and down using the mouse
{
	FRotator NewRotation = GetActorRotation();
	
	
	if ((GetActorRotation().Pitch > -45 || AxisValue >=0) && (GetActorRotation().Pitch < 70 || AxisValue <= 0))//limits the camera to a vertical rotation
	{
		NewRotation.Pitch += AxisValue;
		SetActorRotation(NewRotation);
	}
}

void AComponentPawn::ParticleToggle()//FLAME ON! (Default Space)
{
	if (OurParticleSystem && OurParticleSystem->Template)
	{
		OurParticleSystem->ToggleActive();
	}
}

void AComponentPawn::Release() {
	for (AActor* actor : heldActors) {
		TArray<UCapsuleComponent*> Components;
		actor->GetComponents<UCapsuleComponent>(Components);//Get that capsule component

		actor->DetachFromActor(FDetachmentTransformRules(EDetachmentRule::KeepWorld, true));	//Be gone with you fowl knave!	
		actor->SetActorEnableCollision(true);//re-enable collision. Without this you'll just be a ghost
		
		//old crummy functionality that only worked on Penmen
	/*	if(Components.Num() > 0){//Nobody likes out of bounds array exceptions
		Components[0]->SetSimulatePhysics(true);//#Physics
		Components[0]->AddImpulse(GetActorRotation().Vector() * 1000 * Components[0]->GetMass());//"Put down"... "gently"*/

		//New awesome functionality
		//Wow! I can pick up just about anything now!
		Cast<UPrimitiveComponent>(actor->GetRootComponent())->SetSimulatePhysics(true);
		Cast<UPrimitiveComponent>(actor->GetRootComponent())->AddImpulse(GetActorRotation().Vector() * 1000 * Cast<UPrimitiveComponent>(actor->GetRootComponent())->GetMass());//"Put down"... "gently"
		
		//This is a bug fix... But it's so much more fun when it is disabled!  Failing to remove the actor from the array means you can continue to apply force to the object by pressing 'F' even after the object has been released.	
		//An additional consequence of this functionality/feature/bug is that if the actor is not fully released (IE its stuck on the back of the player) the force compounds until it is finally released with an extra "UMPF!"
		//heldActors.Remove(actor);//Release it from your control		
		//}
	}
}

