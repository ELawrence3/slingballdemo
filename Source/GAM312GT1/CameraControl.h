// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Engine.h"
#include "GameFramework/Actor.h"
#include "CameraControl.generated.h"

UCLASS()
class GAM312GT1_API ACameraControl : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACameraControl();	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera Selection")
	AActor* cameraOne;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera Selection")//this is needed for every variable desired to be accessible from the blueprint
	AActor* cameraTwo;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera Selection")
	AActor* fixedCamera;


	//seperate declarations required for functions and variables.
	//sets camera to detached camera one (default keyboard 1)
	void CameraOne(float Value);
	//sets camera to detached camera two (default keyboard 2)
	void CameraTwo(float Value);
	//sets camera to third person view (default keyboard 3)
	void ThirdPerson();
	//sets camera to first person view (default keyboard 4)
	void FirstPerson();


protected:
	//the following vectors are for storing the proper relative location of the camera for first and third person views. This prevents the need for a second camera component.
	FVector FPV = FVector(0, 0, 80);//relative location vector for first person view
	FVector TPV = FVector(-400, 0, 80);//relative location vector for third person view	

	//variables must be declared in header	
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	void SetupPlayerInputComponent(class UInputComponent* InputComponent);
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	APlayerController* KeyPlayerController;
};