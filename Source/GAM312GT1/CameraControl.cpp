// Fill out your copyright notice in the Description page of Project Settings.

#include "CameraControl.h"
#include "Components/InputComponent.h"
#include "Kismet/GameplayStatics.h"//added required include

void ACameraControl::BeginPlay()
{
	ACameraControl::KeyPlayerController = UGameplayStatics::GetPlayerController(this, 0);//This gets the PlayerController, a type of Controller, which can then be used to replace the erroneous uses of "Controller" in the following functions.
	ACameraControl::SetupPlayerInputComponent(KeyPlayerController->InputComponent);
	Super::BeginPlay();	
}

void ACameraControl::SetupPlayerInputComponent(class UInputComponent* InputComponent)//added missing space between class and uiinput...
{
	check(InputComponent);
	//Changed FixedCamera to ThirdPerson and FirstPerson camera, changed from BindAxis to BindAction, as the button is digital, not analog, theoretically saving memory.
	InputComponent->BindAxis("CameraOne", this, &ACameraControl::CameraOne);//fixed weird quotes//
	InputComponent->BindAxis("CameraTwo", this, &ACameraControl::CameraTwo);//fixed weird quotes
	InputComponent->BindAction("ThirdPerson", IE_Pressed, this,  &ACameraControl::ThirdPerson);//Called when ThirdPerson event fires, default number 3 on keyboard
	InputComponent->BindAction("FirstPerson", IE_Pressed, this,  &ACameraControl::FirstPerson); //Called when FirstPerson event fires, default number 4 on keyboard
}
//changes active camera to camera one
void ACameraControl::CameraOne(float Value)//lowercase v in void, ACameraControl not ACharacter
{
	
	if(KeyPlayerController && Value)//changed Controller, which is pawn only, to KeyPlayerController
	{
		KeyPlayerController->SetViewTarget(cameraOne);//lower case for variable, upper for function
	}
}
//changes active camera to camera two
void ACameraControl::CameraTwo(float Value)
{
	if(KeyPlayerController && Value)//lowercase I in if, //changed Controller, which is pawn only, to KeyPlayerController
	{
		KeyPlayerController->SetViewTarget(cameraTwo);//lower case for variable, upper for function
	}
}

//Sets active camera to camera component attached to player and sets the location to first person vector
void ACameraControl::FirstPerson()
{
	TArray<UCameraComponent*> cams;
	fixedCamera->GetComponents<UCameraComponent>(cams);

	FVector FPV = FVector(0, 0, 80);//relative location vector for first person view

	if(KeyPlayerController)//changed Controller, which is pawn only, to KeyPlayerController
	{

		//first attempt at creating an algebreic follow camera		
/*		FVector eye = KeyPlayerController->GetParentActor()->GetActorLocation() - KeyPlayerController->GetActorForwardVector() + KeyPlayerController->GetParentActor()->GetActorUpVector();		
		FVector cameraForward = KeyPlayerController->GetParentActor()->GetActorLocation() - eye;

		FVector cameraLeft = KeyPlayerController->GetParentActor()->GetActorUpVector() * cameraForward;
		cameraLeft.Normalize();

		FVector cameraUp = cameraForward * cameraLeft;
		cameraUp.Normalize();

		fixedCamera->SetActorLocation(eye);*/		
						
		/*//debugging
		if (cams.Num() > 0)
		{
			UCameraComponent* foundCam = cams[0];
			GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Orange, FString::Printf(TEXT("My Location is: %s"), *foundCam->RelativeLocation.ToString()));
		}*/

		cams[0]->SetRelativeLocation(ACameraControl::FPV);

		KeyPlayerController->SetViewTargetWithBlend(cams[0]->GetOwner(), 0.75f);//
		//KeyPlayerController->SetViewTarget(fixedCamera);//lower case for variable, upper for function		
	}

}
//Sets active camera to camera component attached to player and sets the location to third person vector
void ACameraControl::ThirdPerson()
{
	TArray<UCameraComponent*> cams;
	fixedCamera->GetComponents<UCameraComponent>(cams);

	AActor* camActorOne = (AActor*)cams[0];
	AActor* camActorTwo = (AActor*)cams[1];
	
	

	if (KeyPlayerController)//changed Controller, which is pawn only, to KeyPlayerController
	{
		cams[0]->SetRelativeLocation(ACameraControl::TPV);
		//KeyPlayerController->SetViewTarget(cams[0]->GetOwner());//lower case for variable, upper for function

		KeyPlayerController->SetViewTargetWithBlend(cams[0]->GetOwner(), 0.75f);//
	}

}

// Sets default values
ACameraControl::ACameraControl()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}
// Called every frame
void ACameraControl::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}


